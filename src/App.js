import React from 'react';
import Data from "./TF1-2021-06-30.json";
import './App.css';
import insta from './insta.png';
import thumb from './thumb.png';

import ReactReadMoreReadLess from "react-read-more-read-less";


function App() {
  
  return (

   <div className="App">
     <div className="header">
     <h1>Posts</h1>
      <input placeholder="Search" />
      </div>
		<div className="posts">
      
      {Data.map(post  => {
        return (
          <div key = {post.post_id} className="post">
            <img src={insta} alt="insta" width="30px"height="30px" />
              <p style={{width:"65%"}}>
              <ReactReadMoreReadLess
               
                charLimit={200}
                readMoreText={"Read more "}
                readLessText={"Read less "}
            >
                {post.post_text}
                </ReactReadMoreReadLess>
                
                </p><br></br>
              <p>{post.post_comments.length} Comment</p><br></br>
              <p>{post.post_date}</p>
              <img src={thumb} alt="thumb" width="35px"height="35px" />

            </div>
        )
      })}
    </div>
    </div>
      );

}

export default App;
